=== How to build and upload:

``` bash
  # installing requirements
  sudo apt-get install build-essential devscripts ubuntu-dev-tools debhelper \
       dh-make diff patch cdbs quilt gnupg fakeroot lintian  pbuilder piuparts

  # packaging
  cd libmongo-dev-0.6/
  debuild -S -sa
  
  # uploading
  cd ..
  dput ppa:rubybots/ppa *.changes
```
